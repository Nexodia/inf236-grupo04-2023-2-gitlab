const { Router } = require("express");
const router = new Router();

const { crearUsuario, verificarUsuario } = require("../controllers/usuarios");

// Ruta para crear un nuevo usuario
router.post('/registrar', async (req, res) => {
    try {
        const { username, correo, password } = req.body;
        const resultado = await crearUsuario(username, correo, password);
        res.status(201).json({ mensaje: 'Usuario creado con éxito', resultado });
    } catch (error) {
        res.status(500).json({ error: error.message });
    }
});

// Ruta para verificar un usuario existente
router.post('/login', async (req, res) => {
    try {
        const { correo, password } = req.body;
        const usuario = await verificarUsuario(correo, password);
        res.json(usuario);
    } catch (error) {
        res.status(401).json({ error: error.message });
    }
});

module.exports = router;
