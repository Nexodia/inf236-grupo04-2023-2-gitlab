const mysql = require("mysql2/promise")
const bluebird = require('bluebird');


const obtenerProductos = async() => {
    const con = await mysql.createConnection({
        user: process.env.DB_USER,
        password: process.env.DB_PASSWORD,
        database: process.env.DB_NAME,
        port: process.env.DB_PORT,
        host: process.env.DB_HOST,
        Promise: bluebird
    });
    
    try {
        const query = `SELECT * FROM producto;`;
    
        const res = await con.query(query);
        const [products, fields] = res;

        return products;
    } catch(e) {
        throw Error(e);
    } finally {
        con.end();
    }
}

const obtenerProductoPorId = async (id) => {
    const con = await mysql.createConnection({
        user: process.env.DB_USER,
        password: process.env.DB_PASSWORD,
        database: process.env.DB_NAME,
        port: process.env.DB_PORT,
        host: process.env.DB_HOST,
        Promise: bluebird
    });
    
    try {
        const query = 'SELECT * FROM producto WHERE id_producto = ?';
        const [rows, fields] = await con.execute(query, [id]);

        if (rows.length === 0) {
            throw new Error(`No se encontró ningún producto con ID ${id}`);
        }

        return rows[0]; // Devuelve el primer producto encontrado (debería ser único si se filtra por ID)
    } catch (e) {
        throw e; // Propaga el error hacia arriba
    } finally {
        con.end();
    }
}

module.exports = {
    obtenerProductos,
    obtenerProductoPorId
}