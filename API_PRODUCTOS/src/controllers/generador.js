const mysql = require("mysql2/promise");
const dotenv = require("dotenv");
const bluebird = require("bluebird");

dotenv.config();

const getRandomNumber = (min, max, precision) => {
  const randomValue = Math.random() * (max - min) + min;
  return precision ? parseFloat(randomValue.toFixed(precision)) : Math.floor(randomValue);
};

const insertRandomData = async () => {
  const con = await mysql.createConnection({
    user: process.env.DB_USER,
    password: process.env.DB_PASSWORD,
    database: process.env.DB_NAME,
    port: process.env.DB_PORT,
    host: process.env.DB_HOST,
    Promise: bluebird,
  });

  try {
    // Generar datos aleatorios e insertarlos en la tabla "producto"
    const numRows = 10; // Cambia el número de filas que deseas insertar
    const insertData = [];

    for (let i = 0; i < numRows; i++) {
      const nombre = `Producto ${i + 1}`;
      const precio_unitario = getRandomNumber(10, 100, 2);
      const disponible = Math.random() < 0.5 ? 1 : 0;

      insertData.push([nombre, precio_unitario, disponible, getRandomNumber(0, 50), `Generico ${getRandomNumber(1, 5)}`]);
    }

    await con.query(
      "INSERT INTO producto (nombre, precio_unitario, disponible, stock, tipo) VALUES ?",
      [insertData]
    );

    console.log("Datos aleatorios insertados con éxito en la tabla 'producto'.");
  } catch (error) {
    console.error("Error al insertar datos aleatorios:", error);
  } finally {
    con.end();
  }
};

// Ejecuta insertRandomData() varias veces según sea necesario
insertRandomData();
insertRandomData();
insertRandomData();
insertRandomData();
