function showPurchaseOptions(htmlElement) {

    const productName = htmlElement.querySelector("p").innerText;
    const productImg = htmlElement.querySelector("img").src;

    Swal.fire({
        title: `Comprar ${productName}`,
        text: "Selecciona una opción de compra",
        showCancelButton: true,
        imageUrl: productImg,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Comprar ahora',
        cancelButtonText: 'Agregar al carrito'
    })
    .then((result) => {
        if (result.isConfirmed) {
            Swal.fire(
                'Comprado!',
                `${productName} seras re-dirigido al servicio de compras`,
                'success'
            )

        } else if (result.dismiss === Swal.DismissReason.cancel) {
            Swal.fire(
                'Añadido al carrito',
                `${productName} ha sido añadido al carrito de compras.`,
                'success'
            )
        }
    })
}