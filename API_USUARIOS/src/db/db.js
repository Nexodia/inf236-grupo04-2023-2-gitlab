const mysql = require("mysql2/promise");
const bluebird = require('bluebird');

const create_table_queries = [
    `
    CREATE TABLE IF NOT EXISTS Usuarios (
        id INT AUTO_INCREMENT PRIMARY KEY,
        username VARCHAR(50) UNIQUE,
        correo VARCHAR(100) UNIQUE,
        password VARCHAR(255)
    );
    `
];

const createTables = async () => {
    const con = await mysql.createConnection({
        user: process.env.DB_USER,
        password: process.env.DB_PASSWORD,
        database: process.env.DB_NAME,
        port: process.env.DB_PORT,
        host: process.env.DB_HOST,
        Promise: bluebird
    });

    try {
        for (const table_query of create_table_queries) {
            console.log(`[DB] Creando la tabla si no existe`)
            await con.query(table_query);
        }
        console.log("[DB] Todas las tablas fueron creadas con éxito");
    } catch (e) {
        console.log("[DB] Error :(");
        console.log(e);
        return false;
    } finally {
        await con.end();
    }

    return true;
}

createTables();
