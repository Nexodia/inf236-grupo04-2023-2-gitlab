const mysql = require("mysql2/promise")
const bluebird = require('bluebird');

// Running the file for the correct executing of the code
const create_table_queries = [
    `
    CREATE TABLE IF NOT EXISTS cliente (
        id_cliente INT AUTO_INCREMENT PRIMARY KEY,
        rut VARCHAR(20),
        telefono VARCHAR(20),
        direccion VARCHAR(50),
        nombre VARCHAR(50)
    );
    `,
    `CREATE TABLE IF NOT EXISTS insumo (
        id_insumo INT AUTO_INCREMENT PRIMARY KEY,
        nombre VARCHAR(50),
        tipo VARCHAR(20),
        stock INT
    );
    `,
    `
    CREATE TABLE IF NOT EXISTS empleado (
        id_empleado INT AUTO_INCREMENT PRIMARY KEY,
        rut_empleado VARCHAR(20),
        nombre VARCHAR(50),
        telefono VARCHAR(20)
    );
    `,
    `
    CREATE TABLE IF NOT EXISTS encargo (
        id_encargo INT AUTO_INCREMENT PRIMARY KEY,
        fecha_pedido DATE,
        fecha_esperada_entrega DATE,
        detalles TEXT,
        estado VARCHAR(20),
        id_cliente INT,
        id_comprobante_pago INT,
        FOREIGN KEY (id_comprobante_pago) REFERENCES comprobante_pago(id_comprobante_pago),
        FOREIGN KEY (id_cliente) REFERENCES cliente(id_cliente)
    );
    `,
    `
    CREATE TABLE IF NOT EXISTS comprobante_pago (
        id_comprobante_pago INT AUTO_INCREMENT PRIMARY KEY,
        medio_de_pago VARCHAR(20),
        monto_total DECIMAL(10, 2)
    );
    `,
    `
    CREATE TABLE IF NOT EXISTS producto (
        id_producto INT AUTO_INCREMENT PRIMARY KEY,
        nombre VARCHAR(50),
        precio_unitario DECIMAL(10, 2),
        disponible TINYINT(1) DEFAULT 1,
        tipo VARCHAR(40),
        stock INT
    );
    `,
    `
    CREATE TABLE IF NOT EXISTS encargo_tiene_producto (
        id_producto INT,
        id_encargo INT,
        PRIMARY KEY (id_producto, id_encargo),
        FOREIGN KEY (id_producto) REFERENCES producto(id_producto),
        FOREIGN KEY (id_encargo) REFERENCES encargo(id_encargo)
    );
    `,
    `
    CREATE TABLE IF NOT EXISTS empleado_hace_encargo (
        id_encargo INT,
        id_empleado INT,
        PRIMARY KEY (id_encargo, id_empleado),
        FOREIGN KEY (id_encargo) REFERENCES encargo(id_encargo),
        FOREIGN KEY (id_empleado) REFERENCES empleado(id_empleado)
    );
    `,
    `
    CREATE TABLE IF NOT EXISTS producto_tiene_insumo (
        id_producto INT,
        id_insumo INT,
        PRIMARY KEY (id_producto, id_insumo),
        FOREIGN KEY (id_producto) REFERENCES producto(id_producto),
        FOREIGN KEY (id_insumo) REFERENCES insumo(id_insumo)
    );
    `
];


// Returns true if all tables are created successfully
const createTables = async () => {
    const con = await mysql.createConnection({
        user: process.env.DB_USER,
        password: process.env.DB_PASSWORD,
        database: process.env.DB_NAME,
        port: process.env.DB_PORT,
        host: process.env.DB_HOST,
        Promise: bluebird
    });

    
    try {
        create_table_queries.forEach( async (table_query, i) => {
            console.log(`[DB] Creando la tabla si no existe (${i})`)
            await con.query(table_query);
        });
    } catch (e) {
        console.log("[DB] Error :(")
        console.log(e);
        return false;
    }

    return true;
}

createTables();