document.getElementById('formularioLogin').addEventListener('submit', async (event) => {
    event.preventDefault(); // Previene el envío normal del formulario

    const formData = {
        correo: document.getElementById('email').value,
        password: document.getElementById('password').value
    };

    // Validación de longitud mínima
    if (formData.correo.length < 3 || formData.password.length < 3) {
        Swal.fire({
            icon: 'error',
            title: 'Error en el formulario',
            text: 'Correo electrónico y contraseña deben tener al menos 3 caracteres.'
        });
        return;
    }

    // Validación de formato de correo electrónico
    const correoRegex = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
    if (!correoRegex.test(formData.correo)) {
        Swal.fire({
            icon: 'error',
            title: 'Error en el formulario',
            text: 'Por favor, ingrese un correo electrónico válido.'
        });
        return;
    }

    try {
        const response = await fetch('http://localhost:9090/api/login', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(formData)
        });

        const data = await response.json();

        if (response.ok) {
            Swal.fire({
                icon: 'success',
                title: 'Inicio de Sesión Exitoso',
                text: 'Ha iniciado sesión correctamente.'
            });
            // Aquí puedes redirigir al usuario o manejar el inicio de sesión como prefieras

            console.log(data)

            try {
                console.log(data);
                setTimeout( () => {
                    window.location = `http://localhost:8000/loguear?username=${data["username"]}&correo=${data["correo"]}`; 
                }, 2000)
            }

            catch (e) {
                console.log(e);
            }

        } else {
            Swal.fire({
                icon: 'error',
                title: 'Error en el Inicio de Sesión',
                text: data.error || 'Las credenciales no son validas.'
            });
        }
    } catch (error) {
        Swal.fire({
            icon: 'error',
            title: 'Error de Conexión',
            text: 'No se pudo conectar con el servidor.'
        });
    }
});
