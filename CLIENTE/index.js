const express = require("express");
const cors = require("cors");
const app = express();
const dotenv = require("dotenv");

const session = require('express-session');
const bodyParser = require('body-parser');

dotenv.config();

const PORT = process.env.PORT;

app.set("view engine", "ejs");

app.use(session({
    secret: process.env.SESSION_SECRET,
    resave: false,
    saveUninitialized: true
}));

app.use(cors());

app.use(express.static('public'));

app.use(bodyParser.urlencoded({ extended: true }));

app.get("/", (req, res) => {
    res.render("pages/inicio", req.session);
});

app.get("/insumos", (req, res) => {
    res.render("pages/insumos", req.session);
});

app.get("/login", (req, res) => {
    res.render("pages/login", req.session);
});

app.get("/loguear", async (req, res) => {
    try {
            req.session.username = req.query.username;
            req.session.correo = req.query.correo;
            req.session.loggedIn = true;

            console.log(req.session)

            res.redirect("/marketplace");
    } catch(e) {
        console.log(e);
        res.status(500).json( {
            error: e
        });
    }
});

app.get("/logout", async (req, res) => {
    req.session.destroy();
    res.redirect("/login")
});

app.get("/marketplace", (req, res) => {
    res.render("pages/marketplace", req.session);
});

app.get("/register", (req, res) => {
    res.render("pages/register", req.session);
});

app.get("/carrito", (req, res) => {
    res.send("Falta implementar carrito");
})

app.get("/logout", (req, res) => {
    res.send("Deberia desloguearse aqui");
});

app.listen(PORT, () => {
    console.log(`🚀  CLIENTE está corriendo en el puerto ${PORT}  🚀`);
})