# INF236-GRUPO4-2023-2
Este es el repositorio del grupo 4, cuyos integrantes son:
* Gabriel Apablaza - 202173110-5
* Diego Concha - 202004053-2
* Sebastián Torrealba - 202173027-3 
* Joaquín Véliz - 202004065-6

## Wiki
Puede acceder a la Wiki mediante el siguiente [enlace](https://gitlab.com/Nexodia/inf236-grupo04-2023-2-gitlab/-/wikis/home).

## Videos
* [Video Presentación Cliente](https://www.youtube.com/watch?v=LHmE2B2Bumw&ab_channel=WladimirOrmaz%C3%A1bal)
* [Video Presentación Hito 1](https://youtu.be/mNXmX0tM7eg)
* [Video Presentación Hito 4](https://www.youtube.com/watch?v=4xToSu4Qyp4)
* [Video Presentación Hito 7](https://www.youtube.com/watch?v=LRnpxiiiHEM)


# INF236-2023-2-Proyecto Base

## Aspectos técnicos relevantes

La API está corriendo en un servidor Docker usando Alpine Linux. 

Por parte del backend se estará utilizando NodeJS con ExpressJS.

En el frontend, como framework de CSS se ocupará TailwindCSS, un framework liviano que permite estandarizar la forma que crearemos los componentes. Por parte de la interactividad, la librería de Javascript que ocuparemos será AlpineJS, permite crear interfaces dinámicas sin la necesidad de una alta complejidad en código, perfecto para un proyecto de este estilo.

## Requerimientos

Para utilizar el proyecto base debe tener instalado [Node.js](https://nodejs.org/en), [Docker](https://www.docker.com/) y se recomienda [Postman](https://www.postman.com/) para poder probar los endpoints de la api.

## Puntos a Considerar
La solución a desarrollar debe seguir los siguientes lineamientos (imagen referecial al final):
* Se debe considerar dos API's:
    * API_PRODUCTOS: Con todo lo referido a los productos.
    * API_{DEFINIR}: Con todo lo referido al microservicio que haya definido el grupo.
* Cada API contará con una base de datos mysql.
* Las API's deben ser construidas utilizando [Node.js](https://nodejs.org/en)
![Modelo](img/diagrama.png)
## Levantando el proyecto
### Bases de Datos
Iniciaremos levantando la imagen de mysql en docker. En una terminal escriba el siguiente comando:
```
docker run -p 3306:3306 --name mysql -e MYSQL_ROOT_PASSWORD=password -d mysql
```
Deben esperar a que se inicie en su totalidad el contenedor. Pueden verificar esto en la interfaz de Docker. Si utilizan el comando que sigue sin esperar les mostrará un error.
![Docker](img/console_docker.png)

Una vez creada la imagen, crearemos las bases de datos. Para esto debemos entrar en el contendor, con el siguiente comando:
```
docker exec -it mysql mysql -uroot -p
```
Deben ingresar la clave que en este caso es **password**.

Una vez dentro del contenedor podemos crear las bases de datos. 
```
create database Nombre;
```
Donde Nombre, deberá ser sustituido por: BDXX_PRODUCTOS, BDXX_{DEFINIR}, donde XX corresponde al número de su grupo.

### API's
Deben crear un archivo con el nombre .env en la carpeta API_PRODUCTOS. Este debe contener lo siguiente:
```js
PORT_API = 8080
DB_USER = "root"
DB_PASSWORD = "password"
DB_NAME = "Nombre de la base de datos"
DB_PORT = 3306
DB_HOST = "host.docker.internal"
```
***Nota: deben cambiar DB_NAME por el nombre que le pusieron a la base de datos (BDXX_PRODUCTOS)***

Una vez creado el archivo, deben ir a la terminal (deben estar dentro de la carpeta API_PRODUCTOS) y levantar el contenedor.
```
docker-compose up --build -d
```
Una vez levantado deberían poder ver en docker sus contenedores corriendo, pueden probar el siguiente end-point en postman para verificarlo.
```
GET: localhost:8080/createTable
```
![Ejemplo](img/postman_ejemplo.png)

**Deben replicar este proceso para la API_{DEFINIR}.**

### Enjoy!
