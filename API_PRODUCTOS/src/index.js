const express = require("express");
const morgan = require("morgan");
const dotenv = require("dotenv");
const path = require("path");
const cors = require("cors");

// Routes
const routes = require("./routes/index");

// Database
dotenv.config({ path: path.resolve(__dirname, "../.env") });

const database = require("./db/db");

// App
const app = express();

// Middlewares
app.use(morgan('dev'));
app.use(express.json());

app.use(cors());
app.use("/api", routes);

// Listening server
app.listen(process.env.PORT_API, () => {
    console.log(`🚀  API_PRODUCTOS está corriendo en el puerto ${process.env.PORT_API}  🚀`);
});

const test = async () => {
    const res = await require("./controllers/productos.js").obtenerProductos()
    console.log(res);
    return res;
}
setTimeout( () => {
    test()
    require("./controllers/generador.js");
}, 2000)