document.getElementById('formularioRegistro').addEventListener('submit', async (event) => {
    event.preventDefault(); // Previene el envío normal del formulario

    const formData = {
        username: document.getElementById('username').value,
        correo: document.getElementById('correo').value,
        password: document.getElementById('password').value,
        passwordConfirm: document.getElementById('password-confirm').value
    };

    // Validación de longitud mínima
    if (formData.username.length < 3 || formData.correo.length < 3 || formData.password.length < 3) {
        Swal.fire({
            icon: 'error',
            title: 'Error en el formulario',
            text: 'Todos los campos deben tener al menos 3 caracteres.'
        });
        return;
    }

    // Validación de formato de correo electrónico
    const correoRegex = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
    if (!correoRegex.test(formData.correo)) {
        Swal.fire({
            icon: 'error',
            title: 'Error en el formulario',
            text: 'Por favor, ingrese un correo electrónico válido.'
        });
        return;
    }

    // Validación de coincidencia de contraseñas
    if (formData.password !== formData.passwordConfirm) {
        Swal.fire({
            icon: 'error',
            title: 'Error en el formulario',
            text: 'Las contraseñas no coinciden.'
        });
        return;
    }

    delete formData.passwordConfirm;

    try {
        const response = await fetch('http://localhost:9090/api/registrar', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(formData)
        });

        const data = await response.json();

        if (response.ok) {
            Swal.fire({
                icon: 'success',
                title: 'Registro Exitoso',
                text: 'Usuario registrado con éxito.'
            });
        } else {
            Swal.fire({
                icon: 'error',
                title: 'Error en el Registro',
                text: data.error || 'Ocurrió un error al registrar el usuario.'
            });
        }
    } catch (error) {
        Swal.fire({
            icon: 'error',
            title: 'Error de Conexión',
            text: 'No se pudo conectar con el servidor.'
        });
    }

});
