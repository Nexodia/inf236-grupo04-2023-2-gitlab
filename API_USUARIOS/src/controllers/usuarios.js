const mysql = require("mysql2/promise");
const bcrypt = require('bcrypt');
const bluebird = require('bluebird');

const crearUsuario = async (username, correo, password) => {
    const con = await mysql.createConnection({
        user: process.env.DB_USER,
        password: process.env.DB_PASSWORD,
        database: process.env.DB_NAME,
        port: process.env.DB_PORT,
        host: process.env.DB_HOST,
        Promise: bluebird
    });

    console.log(username, correo, password, "lol")

    try {
        // Verificar si ya existe un usuario con el mismo correo
        const queryVerificacion = 'SELECT * FROM Usuarios WHERE correo = ?';
        const [usuariosExistentes] = await con.execute(queryVerificacion, [correo]);

        if (usuariosExistentes.length > 0) {
            throw new Error('Ya existe un usuario con ese correo electrónico');
        }

        // Hash de la contraseña
        const hashedPassword = await bcrypt.hash(password, 10);

        const queryInsercion = 'INSERT INTO Usuarios (username, correo, password) VALUES (?, ?, ?)';
        const result = await con.execute(queryInsercion, [username, correo, hashedPassword]);

        return result; // Devuelve el resultado de la operación
    } catch (e) {
        throw e; // Propaga el error hacia arriba
    } finally {
        con.end();
    }
}

const verificarUsuario = async (correo, password) => {
    const con = await mysql.createConnection({
        user: process.env.DB_USER,
        password: process.env.DB_PASSWORD,
        database: process.env.DB_NAME,
        port: process.env.DB_PORT,
        host: process.env.DB_HOST,
        Promise: bluebird
    });

    try {
        // Utiliza 'execute' para la preparación automática de la declaración SQL
        const query = 'SELECT * FROM Usuarios WHERE correo = ?';
        const [rows] = await con.execute(query, [correo]);

        if (rows.length === 0) {
            throw new Error(`Usuario no encontrado con el correo: ${correo}`);
        }

        const user = rows[0];

        // Comparar la contraseña proporcionada con la almacenada (hasheada)
        const match = await bcrypt.compare(password, user.password);

        if (!match) {
            throw new Error('Contraseña incorrecta');
        }

        return user; // Devuelve el usuario si la contraseña coincide
    } catch (e) {
        throw e; // Propaga el error hacia arriba
    } finally {
        con.end();
    }
}

module.exports = {
    crearUsuario,
    verificarUsuario
}