const { Router } = require("express");
const router = new Router();

// Controladores
const { obtenerProductos, obtenerProductoPorId } = require("../controllers/productos");

// Endpoints
router.get("/obtenerproductos", async (req, res) => {
    
    try {
        const productos = await obtenerProductos();
        res.json({
            productos: productos
        });

    } catch(e) {
        console.log("[ Error ] ", e);
        res.json({
            "error": 500,
            "msg": e
        })

    }

});

// Endpoint para obtener un producto por su ID
router.get("/obtenerproducto/:id", async (req, res) => {
    try {
        const productoId = req.params.id;
        const producto = await obtenerProductoPorId(productoId);
        
        res.json({
            producto: producto
        });

    } catch (e) {
        console.log("[ Error ] ", e);
        res.status(500).json({
            "error": 500,
            "msg": e.message
        });
    }
});


module.exports = router;